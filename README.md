# Evolución de los precios del iPhone

## Descripción

Este es un proyecto sencillo en Python que utiliza recopila los precios de los iPhone desde que tienen Face ID (2017) y genera distintos gráficos que muestran su evolución a lo largo del tiempo.

## Requisitos

- Python 3.x instalado.
- Bibliotecas necesarias (se pueden instalar usando `pip`):
    - Chardet
    - Pandas
    - Matplotlib

## Uso

1. Clona este repositorio:

``` bash
 git clone https://gitlab.com/adrianfl55/precios-iphone.git
 ```

2. Muévete a tu directorio:
``` bash
cd tu-proyecto
```

3. Ejecuta el proyecto:
```bash
python main.py
```




